[TOC]

# JSON类型

JSON类型：数字，字符串，布尔，对象，数组，null

## 数字

JSON中的数字可以是小数，整数，正数，负数，可以使用科学计数法。

```json
{
    "sadSavingAccount":-289,
    "price":22.59,
    "earthsMass":5.972e+24
}
```

## 字符串

JSON的字符串必须且只能用双引号包裹，不能是单引号。

字符串包含控制字符时，仍旧采用转义字符表示法，双反斜线表示一个反斜线。

<font color=red>如果字符串内含有双引号，需要对字符串内的双引号进行转义。</font>

* “Title”:"John Say "Bye"" (错误)
* "Title":"John Say \\"Bye\\"" (正确)

### JSON字符串转义网站

原始：John Say "Bye"

文本："John Say \\"Bye\\""  (使用JSON语法编辑，第一次转义)

编程：\\"John Say \\\\\\"Bye\\\\\\"\\" （使用编程语法编辑，第二次转义）

https://www.bejson.com/   可利用该网站提供的JSON转义工具进行第二次转义

字符串一

```json
{
 "story":"\t once upon a time, in a far away \n there lived a princess."
}
```

字符串二

```json
{
 "story":"\\t once upon a time, in a far away \\n there lived a princess."
}
```

两个Json字符串都是合法的，但表达的字符串是不同的。

## 对象

对象类型是使用逗号分隔的名称-值对构成的集合，并使用花括号{ }包裹。

json表示对象时，属性名(key)必须用双引号包裹，对于Json而言，key名可以是包含空格，#，% 等的字符串，但是为了可移植性，尽量不要使用特殊字符，数字字母下划线最好，为了兼容更多的语言。 

value如果是字符串类型，可以是任何形式任何字符。 value并不需要总被双引号包裹，value是字符串时，必须使用双引号。如果是数字，布尔值，数组，对象，null，这些都不应被双引号包裹。

## 数组

数组是值的集合，每个值都可以是字符串，数字，布尔值，对象或数组中的任何一种，数组必须被方括号[ ]包裹，且值与值之间用逗号隔开。

["str1","str2",null, 5.0] 这种表示对于json是合法的，但是我们应该尽量避免这样做，因为这种json 数组可以被动态语言正确解析成列表数据类型，但是像C#，java，C++这种强类型语言无法解析成泛型数组或泛型列表数据类型。

## null

null表示一个没有值的值，必须是小写。 

# 序列化和反序列化

将内存（断电即失）中的对象（变量）的状态（值）存储起来持久化（硬盘，数据库）称作序列化serialization，反之称作反序列化deserialization。

内存 --> 硬盘   序列化

硬盘 --> 内存   反序列化

# JSON,字面值表示类型法

1. 类型系统的分类

基础类：数字，字符串，二元

简单类：成员都是基础类，重要的信息是属性

复杂类：成员至少包含一个简单类，也可以包含基础类，重要的信息是属性

集合类：重要的信息是存储的元素（基础类，简单类，复杂类，集合），而非属性，它的属性取决于它盛放的元素。

2. 字面值

只有基础类才有字面值形式。

C#的const关键字就是用具名变量表示字面值，消灭魔法数，魔法字符串，编译后const变量都会被替换成字面值。

C#的const只能修饰基元类型。

推出结论：任何类型都是由基础类扩展组成，那么基于字面值可以表示所有类型。JSON就是这种表示法。

# JSON跨语言

编程语言的类型都可以映射到Json的某种类型，通过JSON这个媒介，可以实现不同语言的类型转换和沟通。

# Newtonsoft.Json

## 简介

Newtonsoft.json.dll，也称Json.NET，是C#进行JSON序列化的类库，第三方私人贡献的类库，非微软官方类库。
开源，跨平台，功能丰富，性能强悍，简单易用。该库极受欢迎，成为下载量第一的nuget包，是.NET平台优先选择的Json序列化类库。
官网：https://www.newtonsoft.com/json

## Newtonsoft.Json和System.Text.Json

1. Newtonsoft.Json功能更丰富（优点）
2. Newtonsoft.Json运行内存较高和运行速度较低（缺点，但不重要，因为性能相差不大，且一般不是应用的性能瓶颈）
3. Newtonsoft.Json需要下载，版本不断迭代，带来引用不同版本的dll麻烦（最大的缺点）

# 最简单的序列化

```csharp
class Person
{
    public string Name { get; set; }
    public int Age { get; set; }
}

Person person = new Person()
{
    Name = "Jack",
    Age = 17
};
```

```csharp
string json = JsonConvert.SerializeObject(person);
Console.WriteLine(json);
```

输出：

{"Name":"Jack","Age":17}

# 序列化时设置缩进

```csharp
string json = JsonConvert.SerializeObject(person,Formatting.Indented);
Console.WriteLine(json);
```

输出：
{
     "Name": "Jack",
     "Age": 17
}

# 缩进JSON字符串

```csharp
string jsonIndented = JToken.Parse(jsonNotIndented).ToString(Formatting.Indented);
```

# 缩进的优缺点

优点：缩进后的Json排版好，更易读。
缺点：添加了大量空白字符，导致序列化后得到的Json长度变长，那么存储的成本会变高，CPU处理的时间会变长，网络发送占用的带宽更高，耗时更久。
建议：因为网络资源成本比较高，存储成本与CPU成本相对较低，所以不要缩进用于网络发送的Json，进程可以缩进接收到的Json后再打印日志和储存。

# 非集合类型的序列化

```C#
JsonConvert.SerializeObject(2021);      // 2021
JsonConvert.SerializeObject('A');       // "A"
JsonConvert.SerializeObject(true);      // true
JsonConvert.SerializeObject("2021");    // "2021"
JsonConvert.SerializeObject(null);      // null
JsonConvert.SerializeObject(DateTime.Now); // "2021-02-01T11:04:11.4245398+08:00"
JsonConvert.SerializeObject(TimeSpan.FromSeconds(1024)); // "00:17:04"
JsonConvert.SerializeObject(new Uri(@"C:\floder")); // "C:\\floder"
JsonConvert.SerializeObject(System.Guid.NewGuid()); // "c711acc0-b87b-48fe-809d-82647d8a79df"
JsonConvert.SerializeObject(new { });   // {}
```

# 集合类型的序列化

集合类型序列化时，并不是为了储存其属性信息，而是其包含的元素的信息。这是合理的，想一想，当我们恢复容器元素后，容器的属性自然也相应的恢复。容器类型有价值的信息是其包含的元素。

## 数组

```csharp
JsonConvert.SerializeObject(new int[] { }); // []
JsonConvert.SerializeObject(new List<double> { 3.14, 9.12, 0.67 });
```

输出：

```csharp
[3.14,9.12,0.67]
```

## 字典

```csharp
Dictionary<string, int> points = new Dictionary<string, int>
{
  { "James", 9001 },
  { "Jo", 3474 },
  { "Jess", 11926 }
};
JsonConvert.SerializeObject(points, Formatting.Indented);
```

```csharp
{
      "James": 9001,
      "Jo": 3474,
      "Jess": 11926
}
```

总结：字典会被序列化成对象，key.ToString()是字段名，value是字段值。

## DataTable

```csharp
DataTable table = new DataTable();
table.Columns.Add(new DataColumn("Name", typeof(string)));
table.Columns.Add(new DataColumn("Age", typeof(int)));
table.Rows.Add("Jack", 20);
JsonConvert.SerializeObject(table);
```

输出：[{"Name":"Jack","Age":20}]
总结：DataTable其实就是实例的集合，实例的字段名就是列名，实例的类型就是列的类型。

## DataSet

```csharp
DataSet dataSet = new DataSet("dataSet");
DataTable table = new DataTable();
DataColumn idColumn = new DataColumn("id", typeof(int));
idColumn.AutoIncrement = true;
DataColumn itemColumn = new DataColumn("item");
table.Columns.Add(idColumn);
table.Columns.Add(itemColumn);
dataSet.Tables.Add(table);
for (int i = 0; i < 2; i++)
{
  DataRow newRow = table.NewRow();
  newRow["item"] = "item " + i;
  table.Rows.Add(newRow);
}
dataSet.AcceptChanges();
string json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);
Console.WriteLine(json);
```

输出：

```json
{
    "Table1": [{
            "id": 0,
            "item": "item 0"
        },
        {
            "id": 1,
            "item": "item 1"
        }
    ]
}
```

# 剔除属性

我们可以不序列化我们不感兴趣的属性，这样可以简化JSON，这样可以减少流量的消耗。

1. 增加私有字段 或 删除公开属性
2. 剔除值为null的属性
3. 设置属性的默认值，并剔除值是默认值的属性
4. 剔除指定类型的属性
5. 剔除不满足指定表达式的属性

```csharp
JsonConvert.SerializeObject(new TestClass(), new JsonSerializerSettings()
{
    ContractResolver = new ConcreteContractResolver()
});
```

```csharp
internal class ConcreteContractResolver : DefaultContractResolver
{
    protected override JsonContract CreateContract(Type objectType)
    {
        return base.CreateContract(objectType);
    }

    protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
    {
        IList<JsonProperty> properties = base.CreateProperties(type, memberSerialization);
        foreach (var item in properties)
        {
            item.PropertyName = "_" + item.PropertyName;
        }
        return properties;
    }

    protected override List<MemberInfo> GetSerializableMembers(Type objectType)
    {
        List<MemberInfo> members = base.GetSerializableMembers(objectType);
        if (objectType.Name == typeof(TestClass).Name)
        {
            FieldInfo fieldInfo = objectType.GetField("_param", BindingFlags.NonPublic | BindingFlags.Instance);
            // 添加私有字段
            members.Add(fieldInfo);
            // 删除属性
            members.RemoveAll(elem => elem.Name == "PropertyRemoved");
        }
        return members;
    }

    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
        JsonProperty jp = base.CreateProperty(member, memberSerialization);

        // 私有属性的Readable必须设置成true才能序列化
        if (jp.PropertyName == "_param")
        {
            jp.Readable = true;
        }

        // 忽略值为默认值的属性 | TestClass的成员Key的值若是DELETE则不序列化
        if (member.DeclaringType == typeof(TestClass))
        {
            if (member.Name == "Key")
            {
                // 设置属性的默认值
                jp.DefaultValue = "DELETE";
                // 忽略值为默认值的属性
                jp.DefaultValueHandling = DefaultValueHandling.Ignore;
            }
        }

        // 忽略值为null的属性
        jp.NullValueHandling = NullValueHandling.Ignore;
        // 忽略值为默认值的属性
        jp.DefaultValueHandling = DefaultValueHandling.Ignore;
        // 忽略特定名称的属性
        if (member.Name == "Id")
        {
            jp.Ignored = true;
        }

        // 忽略指定类型的属性
        if (jp.PropertyType == typeof(bool))
        {
            jp.Ignored = true;
        }

        // 忽略满足指定条件的属性
        jp.ShouldSerialize = new Predicate<object>((obj) =>
        {
            if (member.DeclaringType == typeof(TestClass))
            {
                TestClass @params = obj as TestClass;
                if (@params.Id == 3)
                {
                    return false;
                }
                return true;
            }
            return true;
        });

        return jp;
    }
}
```

# 自定义属性名称

JSON是跨语言跨平台的，经常用于进程间信息交互。常见的B-S模型，前端是Javascript，后端是PHP，C#，python等，如果前后端通过Json格式消息交互，但是不同的语言有着不同的命名习惯，导致序列化后得到的Json字段名称各不相同。比如C#序列化后的Json字段是大驼峰命名法，Json消息发送到前端，Javascript的字段都是小写字母，前端收到后无法正常解析，这时，能够指定序列化的Json字段名就非常有必要了。

### 小驼峰

```csharp
string json = JsonConvert.SerializeObject(new Message() { Params = new Params() }, new JsonSerializerSettings()
{
 	ContractResolver = new DefaultContractResolver() { NamingStrategy = new CamelCaseNamingStrategy() },
});
Console.WriteLine(json);
```

输出：

{"id":0,"key":null,"executeResult":false,"params":{"param1":"\u0000","param2":0.0}}

### 蛇形

```csharp
string json = JsonConvert.SerializeObject(new Message() { Params = new Params() }, new JsonSerializerSettings()
{
 	ContractResolver = new DefaultContractResolver() { NamingStrategy = new SnakeCaseNamingStrategy() },
});
Console.WriteLine(json);
```

输出：

{"id":0,"key":null,"execute_result":false,"params":{"param1":"\u0000","param2":0.0}}

### 单独设置某个属性的名称

单独设置的某个属性的名称，优先级较高，会屏蔽NamingStrategy。

```csharp
internal class ConcreteContractResolver : DefaultContractResolver

{
    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)

    {
        // 注意：如果通过NamingStrategy设置了小驼峰或蛇形，此时的jp的PropertyName已经不是C#的大驼峰，而是小驼峰或蛇形了

        JsonProperty jp = base.CreateProperty(member, memberSerialization);

        if (member.DeclaringType == typeof(Params))

        {
            if (member.Name == "Param1")

            {
                jp.PropertyName = "param_one";
            }
        }

        return jp;
    }
}
```

```csharp
string json = JsonConvert.SerializeObject(new Message() { Params = new Params() }, new JsonSerializerSettings()
{
	ContractResolver = new ConcreteContractResolver() { NamingStrategy = new SnakeCaseNamingStrategy() },
});
```

输出：

{"id":0,"key":null,"execute_result":false,"params":{"param_one":"\u0000","param2":0.0}}

